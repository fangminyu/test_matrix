#include <stdio.h>
// #include <cstring>
// #include <memory>
#include <stdlib.h> 
#include <string.h>

#include <sys/time.h>

// extern "C"
// {
#include <immintrin.h>
// }

/**
 * m[3][3]	   |<-   double *    ->|<----------------         double     --------------------------->|
 *	   memory: | c0 | c1 | c2 | c3 | c10 | c11 | c12 | c13 | c21 | c22 | c23 | c30 | c31 | c32 | c33 |
 *	        => | c0 | c1 | c2 | c3 |   //c0 =rows(3) (double**)
 *	    	   | c10| c11| c12| c13|   //c1 ->pointer c10, c10=cols(3),(double*)
 *	    	   | c20| c21| c22| c23|
 *	    	   | c30| c31| c32| c33|
 *
 *	c0-c3: double array index,c0(int)=3, c1 -> c10, c10(int) =3
 *	c11-c12: array;
 */
typedef float *wtk_matrix_t;
#define wtk_debug printf

#define wtk_matrix_rows(m) (*(int *)m)
#define wtk_matrix_cols(m) (*(int *)(m[1]))

typedef float wtk_vector;
typedef double wtk_double_vector_t;
typedef int wtk_int_vector_t;
typedef wtk_vector wtk_vector_t;
#define wtk_round(size,align) \
 ((align)>0 ? (((size)+((align)-1))&(~((align)-1))) : size)
#define wtk_round_16(size) ((((size)&15)==0)? (size) : ((size)+16-((size)&15)))

//#define wtk_round_word(size) ((((size)&(WTK_ALIGNMENT_BIT-1))==0)? (size) : ((size)+WTK_ALIGNMENT_BIT-((size)&(WTK_ALIGNMENT_BIT-1))))
#define wtk_round_word(size) wtk_round_16(size)
#define wtk_vector_type_bytes(size,type) (wtk_round((size+1)*sizeof(type), 16))
#define wtk_vector_bytes(size) wtk_vector_type_bytes(size,float)
#define wtk_matrix_bytes(r, c) wtk_round_word(wtk_vector_bytes(c) * r + wtk_round_word((r + 1) * sizeof(wtk_vector_t *)))


wtk_matrix_t *wtk_matrix_init(char *p, int nrows, int ncols, int randomfill) {
    float **m;
    int csize;
    int i, j;

    m = (float **)p;
    *((int *)p) = nrows;
    csize = wtk_vector_bytes(ncols);
    p += wtk_round_word((nrows + 1) * sizeof(float *));
    printf("csize:%d row0_offset:%d %d\n", csize, (wtk_round_word((nrows + 1) * sizeof(float *))),
        (nrows + 1) * sizeof(float *));
    for (i = 1; i <= nrows; ++i, p += csize) {
        // printf("row %d\n", i);
        *((int *)p) = ncols;
        m[i] = (float *)p;
        if(randomfill == 1)
        {
            for ( j = 1; j <= ncols; j++)
            {
                m[i][j] = 0+1.0*rand()/RAND_MAX *(1-0);
                // if (j<=4)
                // {
                //     printf("%.4f ", m[i][j]);
                // }
            }
            // printf("\n");
        }
    }
    return m;
}

wtk_matrix_t *wtk_matrix_new(int nrows, int ncols, int randomfill) {
    char *p;

    p = (char *)calloc(1, wtk_matrix_bytes(nrows, ncols));
    printf("wtk_matrix_new [%d,%d] size:%d\n", nrows, ncols, wtk_matrix_bytes(nrows, ncols));
    return wtk_matrix_init(p, nrows, ncols, randomfill);
}


typedef unsigned long long int uint64_t;

static uint64_t GetTimeStampInUSec()
{
    struct timeval t;
    gettimeofday(&t, NULL);
    return ((uint64_t)(t.tv_sec)*(uint64_t)1000000 + t.tv_usec);
}

void wtk_flat_matrix_multi(wtk_matrix_t *m, wtk_matrix_t *a, wtk_matrix_t *b)
{
    register float *tpb, *tpm, *pm;
    register float fa;
    float *pb;
    int i, j, row, col, row2, col2;

    row = wtk_matrix_rows(a);
    col = wtk_matrix_cols(a);
    col2 = wtk_matrix_cols(b);
    pb = b[1];
    for (j = 1; j <= row; ++j) {
        fa = a[j][1];
        tpb = pb;
        tpm = m[j];
        if (fa == 0) {
            memset(tpm + 1, 0, col2 << 2);
        } else {
            pm = tpm + col2;
            while (pm - tpm >= 8) {
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
                *(++tpm) = fa * (*(++tpb));
            }
            while (tpm < pm) {
                *(++tpm) = fa * (*(++tpb));
            }
        }
    }
    for (i = 2; i <= col; ++i) {
        pb = b[i];
        for (j = 1; j <= row; ++j) {
            fa = a[j][i];
            if (fa == 0) {
                continue;
            }
            tpb = pb;
            tpm = m[j];
            pm = tpm + col2;
            while (pm - tpm >= 8) {
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
            }
            while (tpm < pm) {
                *(++tpm) += fa * (*(++tpb));
            }
        }
    }
}

// int is_swap = 1;
// int cnt = 0;

// 直接初始化  tpm 然后用一个循环来计算
void wtk_flat_matrix_multi2(wtk_matrix_t *m, wtk_matrix_t *a, wtk_matrix_t *b)
{
    register float *tpb, *tpm, *pm;
    register float fa;
    float *pb;
    int i, j, row, col, row2, col2;

    row = wtk_matrix_rows(a);
    col = wtk_matrix_cols(a);
    col2 = wtk_matrix_cols(b);
    pb = b[1];
    for (j = 1; j <= row; ++j) {
        tpm = m[j];
        memset(tpm + 1, 0, col2 << 2);
    }
    
    for (i = 1; i <= col; ++i) {
        pb = b[i];
        for (j = 1; j <= row; ++j) {
            fa = a[j][i];
            if (fa == 0) {
                continue;
            }
            tpb = pb;
            tpm = m[j];
            pm = tpm + col2;
            while (pm - tpm >= 8) {
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));

                // *(m[j] + 1) += fa * (*(tpb + 1));
                // *(m[j] + 2) += fa * (*(tpb + 2));
                // *(m[j] + 3) += fa * (*(tpb + 3));
                // *(m[j] + 4) += fa * (*(tpb + 4));
                // *(m[j] + 5) += fa * (*(tpb + 5));
                // *(m[j] + 6) += fa * (*(tpb + 6));
                // *(m[j] + 7) += fa * (*(tpb + 7));
                // *(m[j] + 8) += fa * (*(tpb + 8));
            }
            while (tpm < pm) {
                *(++tpm) += fa * (*(++tpb));
            }
        }
        // is_swap = 1;
    }
    // printf("%d\n",cnt);
    // for (j = 1; j <= row; ++j) {
    //     tpm = m[j];
    //     printf("%f %f %f %f\n", *tpm, *(tpm+1), *(tpm+2), *(tpm+3));
    // }
}

void wtk_flat_matrix_multi3(wtk_matrix_t *m, wtk_matrix_t *a, wtk_matrix_t *b)
{
    register float *tpb, *tpm, *pm;
    register float fa;
    float *pb;
    int i, j, row, col, row2, col2;

    row = wtk_matrix_rows(a);
    col = wtk_matrix_cols(a);
    col2 = wtk_matrix_cols(b);
    pb = b[1];
    for (j = 1; j <= row; ++j) {
        tpm = m[j];
        memset(tpm + 1, 0, col2 << 2);
    }

    int col2mod = col2 % 8;
    for (i = 1; i <= col; ++i) {
        pb = b[i];
        for (j = 1; j <= row; ++j) {
            fa = a[j][i];
            if (fa == 0) {
                continue;
            }
            tpb = pb;
            tpm = m[j];
            pm = tpm + col2;
            int loopsize = (col2 + 7) /8;
            switch(col2mod)
            {
                case 0: do { *(++tpm) += fa * (*(++tpb));
                case 7: *(++tpm) += fa * (*(++tpb));
                case 6: *(++tpm) += fa * (*(++tpb));
                case 5: *(++tpm) += fa * (*(++tpb));
                case 4: *(++tpm) += fa * (*(++tpb));
                case 3: *(++tpm) += fa * (*(++tpb));
                case 2: *(++tpm) += fa * (*(++tpb));
                case 1: *(++tpm) += fa * (*(++tpb));
                           } while(--loopsize > 0);
            }
        }
    }
}

void wtk_flat_matrix_multi4(wtk_matrix_t *m, wtk_matrix_t *a, wtk_matrix_t *b)
{
    register float *tpb, *tpm, *pm;
    register float fa;
    float *pb;
    int i, j, row, col, row2, col2;

    row = wtk_matrix_rows(a);
    col = wtk_matrix_cols(a);
    col2 = wtk_matrix_cols(b);
    pb = b[1];
    for (j = 1; j <= row; ++j) {
        tpm = m[j];
        memset(tpm + 1, 0, col2 << 2);
    }
    for (i = 1; i <= col; ++i) {
        pb = b[i];
        for (j = 1; j <= row; ++j) {
            fa = a[j][i];
            if (fa == 0) {
                continue;
            }
            tpb = pb;
            tpm = m[j];
            pm = tpm + col2;
            while (pm - tpm >= 16) {
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
                *(++tpm) += fa * (*(++tpb));
            }
            while (tpm < pm) {
                *(++tpm) += fa * (*(++tpb));
            }
        }
    }
//     for (j = 1; j <= row; ++j) {
//         tpm = m[j];
//         printf("%f %f %f %f\n", *tpm, *(tpm+1), *(tpm+2), *(tpm+3));
// //         0.000000 142.229980 136.101746 140.787659
// // 0.000000 51.499264 48.840252 51.988541
// // 0.000000 23.848618 25.591885 25.886465
// // 0.000000 26.140575 21.256947 0.000000
//     }
}

void wtk_flat_matrix_multi5(wtk_matrix_t *m, wtk_matrix_t *a, wtk_matrix_t *b)
{
    __m256 ymm0, ymm1, ymm2, ymm3, ymm4, ymm5, ymm6, ymm7, 
            ymm8, ymm9, ymm10, ymm11, ymm12, ymm13, ymm14, ymm15;
    register float *tpb, *tpm, *pm;
    register float fa;
    float *pb;
    int i, j, row, col, row2, col2;

    row = wtk_matrix_rows(a);
    col = wtk_matrix_cols(a);
    col2 = wtk_matrix_cols(b);
    pb = b[1];
    for (j = 1; j <= row; ++j) {
        tpm = m[j];
        memset(tpm + 1, 0, col2 << 2);
    }

    float tempresult[8];
    const int SIZE_reduced_32 = col - col%32 + 1;
    for (int i=1; i <= row; i++) {
        for(int j=1; j <= col; j++) {
            float res = 0.0;
            float b_temp[32];
            for (int jj= 1; jj < SIZE_reduced_32; jj +=32) {
                ymm8 = __builtin_ia32_loadups256(&b[jj][j]);
                ymm9 = __builtin_ia32_loadups256(&b[jj+8][j]);
                ymm10 = __builtin_ia32_loadups256(&b[jj+16][j]);
                ymm11 = __builtin_ia32_loadups256(&b[jj+24][j]);

                // printf("jj %d\n", jj);
                ymm0 = __builtin_ia32_loadups256(&a[i][jj]);
                ymm1 = __builtin_ia32_loadups256(&a[i][jj+8]);
                ymm2 = __builtin_ia32_loadups256(&a[i][jj+16]);
                ymm3 = __builtin_ia32_loadups256(&a[i][jj+24]);

                ymm0 = __builtin_ia32_mulps256(ymm0, ymm8 );
                ymm1 = __builtin_ia32_mulps256(ymm1, ymm9 );
                ymm2 = __builtin_ia32_mulps256(ymm2, ymm10);
                ymm3 = __builtin_ia32_mulps256(ymm3, ymm11);

                ymm0 = __builtin_ia32_addps256(ymm0, ymm1);
                ymm2 = __builtin_ia32_addps256(ymm2, ymm3);
                ymm0 = __builtin_ia32_addps256(ymm0, ymm2);

                __builtin_ia32_storeups256(tempresult, ymm0);
                for (int kk=0; kk<8; kk++) {
                    res += tempresult[kk];
                }
            }

            // printf("after 32 >>> %d %d\n", col+1, SIZE_reduced_32);
            // 0.000000 137.616562 135.816910 134.796600
            int rest = SIZE_reduced_32;
            while(rest < col + 1)
            {
                ymm0 = __builtin_ia32_loadups256(&a[i][rest]);
                ymm8 = __builtin_ia32_loadups256(&b[rest][j]);
                ymm0 = __builtin_ia32_mulps256(ymm0, ymm8);
                __builtin_ia32_storeups256(tempresult, ymm0);
                for (int kk=0; kk<8; kk++) {
                    res += tempresult[kk];
                }
                rest += 8;
                // printf("rest %d\n", rest);
            }

            // 0.000000 142.229980 136.101746 140.787659
            // for (int jj=1; jj < col+1; jj++) {
            //     res += a[i][jj] * b[jj][j];
            //     if (i == 1 && j == 1)
            //     {
            //         printf("%f %f %f %d,%d\n", res, a[i][jj], b[j][jj], j, jj);
            //         cnt ++;
            //     }
            // }
            // if (i == 1 && j == 1)
            // {
            //     printf("%d\n",cnt);
            // }
            m[i][j] = res;
        }
    }

    // 用于检查计算结果是否正确
// 0.000000 142.229980 136.101746 140.787659
// 0.000000 51.499264 48.840252 51.988541
// 0.000000 23.848618 25.591885 25.886465
// 0.000000 26.140575 21.256947 0.000000
    for (j = 1; j <= row; ++j) {
        tpm = m[j];
        printf("%f %f %f %f\n", *tpm, *(tpm+1), *(tpm+2), *(tpm+3));
    }
}

// #define CAL_MULTI(cal) cal(m0, a0, b0);cal(m1, a1, b1);cal(m2, a2, b2);cal(m3, a2, b3);
// #define CAL_MULTI(cal) cal(m0, a0, b0);
#define CAL_MULTI(cal) cal(m1, a1, b1);cal(m2, a2, b2);cal(m3, a2, b3);

int main()
{
// wtk_flat.c:wtk_flat_matrix_multi:415:==========[1,540] * [540,211] =[1,211]======== 113940
    wtk_matrix_t* a0 = wtk_matrix_new(1, 540, 1);
    wtk_matrix_t* b0 = wtk_matrix_new(540, 211, 1);
    wtk_matrix_t* m0 = wtk_matrix_new(1, 211, 0);
// wtk_flat.c:wtk_flat_matrix_multi:415:==========[1,211] * [211,96] =[1,96]======== 20256
    wtk_matrix_t* a1 = wtk_matrix_new(1, 211, 1);
    wtk_matrix_t* b1 = wtk_matrix_new(211, 96, 1);
    wtk_matrix_t* m1 = wtk_matrix_new(1, 96, 0);
// wtk_flat.c:wtk_flat_matrix_multi:415:==========[1,96] * [96,348] =[1,348]======== 33408
    wtk_matrix_t* a2 = wtk_matrix_new(1, 96, 1);
    wtk_matrix_t* b2 = wtk_matrix_new(96, 348, 1);
    wtk_matrix_t* m2 = wtk_matrix_new(1, 348, 0);
// wtk_flat.c:wtk_flat_matrix_multi:415:==========[1,348] * [348,2] =[1,2]======== 696
    wtk_matrix_t* a3 = wtk_matrix_new(1, 348, 1);
    wtk_matrix_t* b3 = wtk_matrix_new(348, 2, 1);
    wtk_matrix_t* m3 = wtk_matrix_new(1, 2, 0);

    uint64_t start_timestamp = GetTimeStampInUSec();
    for (int i = 0; i < 2000; i++)
    {
        // CAL_MULTI(wtk_flat_matrix_multi)
        CAL_MULTI(wtk_flat_matrix_multi2)
        // CAL_MULTI(wtk_flat_matrix_multi3)
        // CAL_MULTI(wtk_flat_matrix_multi4)
        // CAL_MULTI(wtk_flat_matrix_multi5)
    }
    printf("cost %llu \n", (GetTimeStampInUSec()-start_timestamp));

    return 0;
} 